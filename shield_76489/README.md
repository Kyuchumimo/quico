## Shield for the MatrixPortal M4

There are two folders:

### sn76489

PCB with all through-hole components, except audio-jack with is SMT but is
as easy to solder as the rest.

Has two ouputs:
* 3.5mm audio-jack
* Audio-out + 5v + GND: to be used by a speaker or amplifier

### sn76489_with_audio_amp

Like "sn76489", but with the audio-amplifier built-in.
Most of the components are going to be SMT though.
