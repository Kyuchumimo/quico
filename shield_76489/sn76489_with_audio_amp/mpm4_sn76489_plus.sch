EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SN76489 shield + Audio Amplifier for MatrixPortal M4"
Date "2020-12-22"
Rev "A"
Comp "Ricardo Quesada"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC595 U1
U 1 1 5FCF98EA
P 7000 1800
F 0 "U1" H 7400 1100 50  0000 C CNN
F 1 "74HC595" H 7500 1000 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7000 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 7000 1800 50  0001 C CNN
F 4 " C5947" H 7000 1800 50  0001 C CNN "LCSC"
	1    7000 1800
	1    0    0    -1  
$EndComp
Text GLabel 8550 2250 2    50   Input ~ 0
MP_A1
Text GLabel 8550 2350 2    50   Input ~ 0
MP_A2
Text GLabel 8550 2450 2    50   Input ~ 0
MP_A3
Text GLabel 8550 2550 2    50   Input ~ 0
MP_A4
Wire Wire Line
	8400 2050 8550 2050
Wire Wire Line
	8400 2150 8550 2150
Wire Wire Line
	8400 2250 8550 2250
Wire Wire Line
	8400 2350 8550 2350
Wire Wire Line
	8400 2450 8550 2450
Wire Wire Line
	8400 2550 8550 2550
Wire Wire Line
	8400 2750 8550 2750
$Comp
L Connector:Conn_01x08_Female J1
U 1 1 5FD19BF1
P 8200 2350
F 0 "J1" H 8092 2835 50  0000 C CNN
F 1 "Conn_01x08_Female" H 8092 2744 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8200 2350 50  0001 C CNN
F 3 "~" H 8200 2350 50  0001 C CNN
	1    8200 2350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8550 2750 8550 2850
$Comp
L power:GND #PWR0101
U 1 1 5FD1E5D1
P 8550 2850
F 0 "#PWR0101" H 8550 2600 50  0001 C CNN
F 1 "GND" H 8555 2677 50  0000 C CNN
F 2 "" H 8550 2850 50  0001 C CNN
F 3 "" H 8550 2850 50  0001 C CNN
	1    8550 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FD1EED3
P 7000 2600
F 0 "#PWR0102" H 7000 2350 50  0001 C CNN
F 1 "GND" H 7005 2427 50  0000 C CNN
F 2 "" H 7000 2600 50  0001 C CNN
F 3 "" H 7000 2600 50  0001 C CNN
	1    7000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 1600 6400 1600
Wire Wire Line
	6600 1400 6400 1400
Wire Wire Line
	5800 1700 5800 1650
Wire Wire Line
	5800 1700 6600 1700
$Comp
L power:+5V #PWR0103
U 1 1 5FD43220
P 5800 1650
F 0 "#PWR0103" H 5800 1500 50  0001 C CNN
F 1 "+5V" H 5815 1823 50  0000 C CNN
F 2 "" H 5800 1650 50  0001 C CNN
F 3 "" H 5800 1650 50  0001 C CNN
	1    5800 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 1900 6400 1900
$Comp
L power:+5V #PWR0104
U 1 1 5FD44056
P 7000 900
F 0 "#PWR0104" H 7000 750 50  0001 C CNN
F 1 "+5V" H 7015 1073 50  0000 C CNN
F 2 "" H 7000 900 50  0001 C CNN
F 3 "" H 7000 900 50  0001 C CNN
	1    7000 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2000 6450 2000
Wire Wire Line
	6450 2000 6450 2100
$Comp
L power:GND #PWR0105
U 1 1 5FD449B6
P 6450 2100
F 0 "#PWR0105" H 6450 1850 50  0001 C CNN
F 1 "GND" H 6455 1927 50  0000 C CNN
F 2 "" H 6450 2100 50  0001 C CNN
F 3 "" H 6450 2100 50  0001 C CNN
	1    6450 2100
	1    0    0    -1  
$EndComp
NoConn ~ 7400 2300
Wire Wire Line
	8400 2650 8550 2650
Wire Wire Line
	7400 1400 7500 1400
Wire Wire Line
	7400 1500 7500 1500
Wire Wire Line
	7400 1600 7500 1600
Wire Wire Line
	7400 1700 7500 1700
Wire Wire Line
	7400 1800 7500 1800
Wire Wire Line
	7400 1900 7500 1900
Wire Wire Line
	7400 2000 7500 2000
Wire Wire Line
	7400 2100 7500 2100
Wire Wire Line
	2750 1700 2650 1700
Wire Wire Line
	2750 1800 2650 1800
Wire Wire Line
	2750 1900 2650 1900
Wire Wire Line
	2750 2000 2650 2000
Wire Wire Line
	2750 2100 2650 2100
Wire Wire Line
	2750 2200 2650 2200
Wire Wire Line
	2750 2300 2650 2300
Wire Wire Line
	2750 2400 2650 2400
Text GLabel 7500 1400 2    50   Input ~ 0
D0
Text GLabel 7500 1500 2    50   Input ~ 0
D1
Text GLabel 7500 1600 2    50   Input ~ 0
D2
Text GLabel 7500 1700 2    50   Input ~ 0
D3
Text GLabel 7500 1800 2    50   Input ~ 0
D4
Text GLabel 7500 1900 2    50   Input ~ 0
D5
Text GLabel 7500 2000 2    50   Input ~ 0
D6
Text GLabel 7500 2100 2    50   Input ~ 0
D7
Text GLabel 2650 1700 0    50   Input ~ 0
D0
Text GLabel 2650 1800 0    50   Input ~ 0
D1
Text GLabel 2650 1900 0    50   Input ~ 0
D2
Text GLabel 2650 2000 0    50   Input ~ 0
D3
Text GLabel 2650 2100 0    50   Input ~ 0
D4
Text GLabel 2650 2200 0    50   Input ~ 0
D5
Text GLabel 2650 2300 0    50   Input ~ 0
D6
Text GLabel 2650 2400 0    50   Input ~ 0
D7
$Comp
L power:GND #PWR0107
U 1 1 5FD5886B
P 3250 2800
F 0 "#PWR0107" H 3250 2550 50  0001 C CNN
F 1 "GND" H 3255 2627 50  0000 C CNN
F 2 "" H 3250 2800 50  0001 C CNN
F 3 "" H 3250 2800 50  0001 C CNN
	1    3250 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2100 3800 2100
Text GLabel 3800 2100 2    50   Input ~ 0
SN_CLK
NoConn ~ 3750 1900
Text GLabel 3800 2300 2    50   Input ~ 0
SN_OUT
Wire Wire Line
	3750 1700 3800 1700
Text GLabel 3800 1700 2    50   Input ~ 0
SN_WE
Text GLabel 9650 2550 0    50   Input ~ 0
MP_A4
Wire Wire Line
	9650 2550 9800 2550
Text GLabel 9800 2550 2    50   Input ~ 0
SN_WE
Text GLabel 9650 2250 0    50   Input ~ 0
MP_A1
Text GLabel 9650 2350 0    50   Input ~ 0
MP_A2
Text GLabel 9650 2450 0    50   Input ~ 0
MP_A3
Wire Wire Line
	9650 2450 9800 2450
Wire Wire Line
	9650 2350 9800 2350
Wire Wire Line
	9650 2250 9800 2250
Text GLabel 9800 2350 2    50   Input ~ 0
595_SER
Text GLabel 9800 2250 2    50   Input ~ 0
595_SRCLK
Text GLabel 9800 2450 2    50   Input ~ 0
595_RCLK
Text GLabel 6400 1400 0    50   Input ~ 0
595_SER
Text GLabel 6400 1600 0    50   Input ~ 0
595_SRCLK
Text GLabel 6400 1900 0    50   Input ~ 0
595_RCLK
Text GLabel 8550 2050 2    50   Input ~ 0
MP_TX
Text GLabel 8550 2150 2    50   Input ~ 0
MP_RX
Text GLabel 8550 2650 2    50   Input ~ 0
MP_3V3
Text GLabel 9700 2650 0    50   Input ~ 0
MP_3V3
Text GLabel 9650 2050 0    50   Input ~ 0
MP_TX
Text GLabel 9650 2150 0    50   Input ~ 0
MP_RX
Wire Wire Line
	9650 2150 9800 2150
Wire Wire Line
	9650 2050 9800 2050
Wire Wire Line
	9700 2650 9800 2650
NoConn ~ 9800 2050
NoConn ~ 9800 2150
NoConn ~ 9800 2650
$Comp
L power:+5V #PWR0108
U 1 1 5FDA4FA9
P 3250 1100
F 0 "#PWR0108" H 3250 950 50  0001 C CNN
F 1 "+5V" H 3265 1273 50  0000 C CNN
F 2 "" H 3250 1100 50  0001 C CNN
F 3 "" H 3250 1100 50  0001 C CNN
	1    3250 1100
	1    0    0    -1  
$EndComp
$Comp
L SN76489:SN76489 U2
U 1 1 5FD002BF
P 3250 2550
F 0 "U2" H 3600 2500 60  0000 C CNN
F 1 "SN76489" H 3750 2400 60  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 3000 3550 60  0001 C CNN
F 3 "" H 3000 3550 60  0001 C CNN
	1    3250 2550
	1    0    0    -1  
$EndComp
$Comp
L Oscillator:ACO-xxxMHz-A X1
U 1 1 5FDAF0AE
P 9100 1200
F 0 "X1" H 9450 1050 50  0000 L CNN
F 1 "3.579545Mhz" H 9450 950 50  0000 L CNN
F 2 "Oscillator:Oscillator_DIP-14" H 9550 850 50  0001 C CNN
F 3 "http://www.conwin.com/datasheets/cx/cx030.pdf" H 8825 1325 50  0001 C CNN
	1    9100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 1200 9450 1200
Text GLabel 9450 1200 2    50   Input ~ 0
SN_CLK
Wire Wire Line
	9100 1500 9100 1550
$Comp
L power:+5V #PWR0110
U 1 1 5FDB81D7
P 9100 700
F 0 "#PWR0110" H 9100 550 50  0001 C CNN
F 1 "+5V" H 9115 873 50  0000 C CNN
F 2 "" H 9100 700 50  0001 C CNN
F 3 "" H 9100 700 50  0001 C CNN
	1    9100 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5FDB873E
P 9100 1550
F 0 "#PWR0111" H 9100 1300 50  0001 C CNN
F 1 "GND" H 9105 1377 50  0000 C CNN
F 2 "" H 9100 1550 50  0001 C CNN
F 3 "" H 9100 1550 50  0001 C CNN
	1    9100 1550
	1    0    0    -1  
$EndComp
NoConn ~ 8600 1200
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FDDB3C5
P 6400 4000
F 0 "H1" V 6354 4150 50  0000 L CNN
F 1 "MountingHole 5V" V 6445 4150 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_ISO7380_Pad_TopBottom" H 6400 4000 50  0001 C CNN
F 3 "~" H 6400 4000 50  0001 C CNN
	1    6400 4000
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FDDC75A
P 6400 4200
F 0 "H2" V 6354 4350 50  0000 L CNN
F 1 "MountingHole GND" V 6445 4350 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_ISO7380_Pad_TopBottom" H 6400 4200 50  0001 C CNN
F 3 "~" H 6400 4200 50  0001 C CNN
	1    6400 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	6300 4200 6100 4200
Wire Wire Line
	6100 4200 6100 4350
Wire Wire Line
	6300 4000 6100 4000
Wire Wire Line
	6100 4000 6100 3900
$Comp
L power:+5V #PWR0113
U 1 1 5FE0EF25
P 6100 3900
F 0 "#PWR0113" H 6100 3750 50  0001 C CNN
F 1 "+5V" H 6115 4073 50  0000 C CNN
F 2 "" H 6100 3900 50  0001 C CNN
F 3 "" H 6100 3900 50  0001 C CNN
	1    6100 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5FE0F363
P 6100 4350
F 0 "#PWR0114" H 6100 4100 50  0001 C CNN
F 1 "GND" H 6105 4177 50  0000 C CNN
F 2 "" H 6100 4350 50  0001 C CNN
F 3 "" H 6100 4350 50  0001 C CNN
	1    6100 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5FE5E2C7
P 3550 1200
F 0 "C2" V 3450 1150 50  0000 L CNN
F 1 "0.1uF" V 3650 1100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3550 1200 50  0001 C CNN
F 3 "~" H 3550 1200 50  0001 C CNN
F 4 "C1790" V 3550 1200 50  0001 C CNN "LCSC"
	1    3550 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 2700 3250 2800
$Comp
L power:GND #PWR0109
U 1 1 5FE90D67
P 3750 1300
F 0 "#PWR0109" H 3750 1050 50  0001 C CNN
F 1 "GND" H 3755 1127 50  0000 C CNN
F 2 "" H 3750 1300 50  0001 C CNN
F 3 "" H 3750 1300 50  0001 C CNN
	1    3750 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2500 7000 2600
Text Notes 5750 3550 0    118  ~ 0
Power
Text Notes 650  750  0    118  ~ 0
Audio
Text Notes 5600 750  0    118  ~ 0
Logic
$Comp
L Amplifier_Audio:PAM8302AAD U3
U 1 1 5FE1B2E5
P 2100 4800
F 0 "U3" H 2450 4450 50  0000 C CNN
F 1 "PAM8302" H 2550 4350 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 2100 4800 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PAM8302A.pdf" H 2100 4800 50  0001 C CNN
F 4 "C112137" H 2100 4800 50  0001 C CNN "LCSC"
	1    2100 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5FE1BC24
P 3750 3600
F 0 "FB1" V 3513 3600 50  0000 C CNN
F 1 "120 Ohm/1A" V 3604 3600 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 3680 3600 50  0001 C CNN
F 3 "~" H 3750 3600 50  0001 C CNN
F 4 "C76809" V 3750 3600 50  0001 C CNN "LCSC"
	1    3750 3600
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5FE1CB9C
P 2300 4300
F 0 "C4" V 2071 4300 50  0000 C CNN
F 1 "0.1uF" V 2162 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2300 4300 50  0001 C CNN
F 3 "~" H 2300 4300 50  0001 C CNN
F 4 "C1790" V 2300 4300 50  0001 C CNN "LCSC"
	1    2300 4300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5FE39D44
P 2100 5300
F 0 "#PWR0106" H 2100 5050 50  0001 C CNN
F 1 "GND" H 2105 5127 50  0000 C CNN
F 2 "" H 2100 5300 50  0001 C CNN
F 3 "" H 2100 5300 50  0001 C CNN
	1    2100 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 5200 2100 5300
$Comp
L Device:C_Small C7
U 1 1 5FE3CD45
P 4250 4050
F 0 "C7" H 4158 4004 50  0000 R CNN
F 1 "220pF" H 4158 4095 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4250 4050 50  0001 C CNN
F 3 "~" H 4250 4050 50  0001 C CNN
F 4 "C53172" H 4250 4050 50  0001 C CNN "LCSC"
	1    4250 4050
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5FE3D0CB
P 4250 3700
F 0 "C6" H 4342 3746 50  0000 L CNN
F 1 "220pF" H 4342 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4250 3700 50  0001 C CNN
F 3 "~" H 4250 3700 50  0001 C CNN
F 4 "C53172" H 4250 3700 50  0001 C CNN "LCSC"
	1    4250 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB2
U 1 1 5FE41895
P 3750 4150
F 0 "FB2" V 4000 4150 50  0000 C CNN
F 1 "120 Ohm/1A" V 3900 4150 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 3680 4150 50  0001 C CNN
F 3 "~" H 3750 4150 50  0001 C CNN
F 4 "C76809" V 3750 4150 50  0001 C CNN "LCSC"
	1    3750 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 4800 2550 4800
Wire Wire Line
	3450 4150 3650 4150
Wire Wire Line
	3450 3600 3650 3600
Wire Wire Line
	3850 4150 4250 4150
Wire Wire Line
	3850 3600 4250 3600
Connection ~ 4250 3600
Wire Wire Line
	4250 3800 4250 3850
Wire Wire Line
	4250 3850 4000 3850
Connection ~ 4250 3850
Wire Wire Line
	4250 3850 4250 3950
$Comp
L power:GND #PWR0115
U 1 1 5FEA902C
P 4000 3850
F 0 "#PWR0115" H 4000 3600 50  0001 C CNN
F 1 "GND" H 4005 3677 50  0000 C CNN
F 2 "" H 4000 3850 50  0001 C CNN
F 3 "" H 4000 3850 50  0001 C CNN
	1    4000 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4150 4650 4150
Connection ~ 4250 4150
Wire Wire Line
	4250 3600 4650 3600
Wire Wire Line
	4650 3600 4650 3800
Wire Wire Line
	4650 4150 4650 3900
Wire Wire Line
	2500 4600 2550 4600
Text GLabel 2550 4600 2    50   Input ~ 0
PAM_OUT+
Text GLabel 2550 4800 2    50   Input ~ 0
PAM_OUT-
Text GLabel 3450 4150 0    50   Input ~ 0
PAM_OUT-
Text GLabel 3450 3600 0    50   Input ~ 0
PAM_OUT+
Text GLabel 1400 4600 0    50   Input ~ 0
PAM_IN+
$Comp
L Device:R_Small R1
U 1 1 5FF09B65
P 2050 3250
F 0 "R1" V 1854 3250 50  0000 C CNN
F 1 "100" V 1945 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2050 3250 50  0001 C CNN
F 3 "~" H 2050 3250 50  0001 C CNN
F 4 "C17408" V 2050 3250 50  0001 C CNN "LCSC"
	1    2050 3250
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5FF0D649
P 1750 3250
F 0 "C5" V 1979 3250 50  0000 C CNN
F 1 "1uF" V 1888 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1750 3250 50  0001 C CNN
F 3 "~" H 1750 3250 50  0001 C CNN
F 4 "C28323" V 1750 3250 50  0001 C CNN "LCSC"
	1    1750 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 3250 1950 3250
Wire Wire Line
	2150 3250 2250 3250
Text GLabel 2250 3250 2    50   Input ~ 0
PAM_IN+
Wire Wire Line
	2100 3950 2200 3950
Wire Wire Line
	2100 3950 2100 4300
Wire Wire Line
	2100 4300 2200 4300
Wire Wire Line
	2400 3950 2450 3950
Wire Wire Line
	2450 3950 2450 4300
Wire Wire Line
	2450 4300 2400 4300
Wire Wire Line
	2450 4300 2650 4300
Connection ~ 2450 4300
Connection ~ 2100 4300
$Comp
L power:GND #PWR0120
U 1 1 5FF61E45
P 2650 4300
F 0 "#PWR0120" H 2650 4050 50  0001 C CNN
F 1 "GND" H 2655 4127 50  0000 C CNN
F 2 "" H 2650 4300 50  0001 C CNN
F 3 "" H 2650 4300 50  0001 C CNN
	1    2650 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4300 2100 4400
Wire Wire Line
	4750 3900 4650 3900
Wire Wire Line
	4750 3800 4650 3800
Wire Wire Line
	3750 2300 3800 2300
Text GLabel 1350 3000 1    50   Input ~ 0
SN_OUT
Wire Wire Line
	1500 3250 1650 3250
Wire Notes Line
	5400 5600 500  5600
Wire Notes Line
	5400 3100 11200 3100
Wire Wire Line
	3750 1200 3750 1300
Wire Wire Line
	3650 1200 3750 1200
Wire Wire Line
	3250 1100 3250 1200
$Comp
L Device:C_Small C8
U 1 1 601C629C
P 7200 950
F 0 "C8" V 7100 900 50  0000 L CNN
F 1 "0.1uF" V 7300 850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7200 950 50  0001 C CNN
F 3 "~" H 7200 950 50  0001 C CNN
F 4 "C1790" V 7200 950 50  0001 C CNN "LCSC"
	1    7200 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 950  7100 950 
Wire Wire Line
	7450 950  7450 1000
$Comp
L power:GND #PWR0123
U 1 1 601CF3B9
P 7450 1000
F 0 "#PWR0123" H 7450 750 50  0001 C CNN
F 1 "GND" H 7455 827 50  0000 C CNN
F 2 "" H 7450 1000 50  0001 C CNN
F 3 "" H 7450 1000 50  0001 C CNN
	1    7450 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 900  7000 950 
$Comp
L Device:C_Small C1
U 1 1 6023847C
P 2300 3950
F 0 "C1" V 2071 3950 50  0000 C CNN
F 1 "10uF" V 2162 3950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2300 3950 50  0001 C CNN
F 3 "~" H 2300 3950 50  0001 C CNN
F 4 "C15850" V 2300 3950 50  0001 C CNN "LCSC"
	1    2300 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 5000 1700 5000
Connection ~ 7000 950 
Wire Wire Line
	7000 950  7000 1200
Wire Wire Line
	7300 950  7450 950 
$Comp
L Device:C_Small C3
U 1 1 602D090C
P 9450 800
F 0 "C3" V 9350 750 50  0000 L CNN
F 1 "0.1uF" V 9550 700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9450 800 50  0001 C CNN
F 3 "~" H 9450 800 50  0001 C CNN
F 4 "C1790" V 9450 800 50  0001 C CNN "LCSC"
	1    9450 800 
	0    1    1    0   
$EndComp
Wire Wire Line
	9550 800  9650 800 
Wire Wire Line
	9650 800  9650 850 
$Comp
L power:GND #PWR0117
U 1 1 602F867E
P 9650 850
F 0 "#PWR0117" H 9650 600 50  0001 C CNN
F 1 "GND" H 9655 677 50  0000 C CNN
F 2 "" H 9650 850 50  0001 C CNN
F 3 "" H 9650 850 50  0001 C CNN
	1    9650 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 900  9100 800 
Wire Wire Line
	9100 800  9350 800 
Wire Wire Line
	9100 800  9100 700 
Connection ~ 9100 800 
Wire Wire Line
	3250 1200 3450 1200
Connection ~ 3250 1200
Wire Wire Line
	3250 1200 3250 1400
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 60317449
P 4950 3800
F 0 "J2" H 5030 3792 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 4700 3550 50  0000 L CNN
F 2 "Connectors:SCREWTERMINAL-3.5MM-2" H 4950 3800 50  0001 C CNN
F 3 "~" H 4950 3800 50  0001 C CNN
	1    4950 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5000 1050 4950
$Comp
L power:+5V #PWR0118
U 1 1 6010423B
P 1050 4950
F 0 "#PWR0118" H 1050 4800 50  0001 C CNN
F 1 "+5V" H 1065 5123 50  0000 C CNN
F 2 "" H 1050 4950 50  0001 C CNN
F 3 "" H 1050 4950 50  0001 C CNN
	1    1050 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3000 1350 3100
Wire Wire Line
	1400 4600 1700 4600
Wire Wire Line
	1350 3400 1350 3550
$Comp
L power:GND #PWR0116
U 1 1 601701B9
P 1350 3550
F 0 "#PWR0116" H 1350 3300 50  0001 C CNN
F 1 "GND" H 1355 3377 50  0000 C CNN
F 2 "" H 1350 3550 50  0001 C CNN
F 3 "" H 1350 3550 50  0001 C CNN
	1    1350 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 4800 1450 4800
Wire Wire Line
	1450 4800 1450 5150
$Comp
L power:GND #PWR0121
U 1 1 60174958
P 1450 5150
F 0 "#PWR0121" H 1450 4900 50  0001 C CNN
F 1 "GND" H 1455 4977 50  0000 C CNN
F 2 "" H 1450 5150 50  0001 C CNN
F 3 "" H 1450 5150 50  0001 C CNN
	1    1450 5150
	1    0    0    -1  
$EndComp
Text GLabel 2850 6150 2    50   Input ~ 0
PAM_IN+
Wire Notes Line
	5400 6850 500  6850
Wire Notes Line
	5400 500  5400 6850
Text Notes 650  5800 0    118  ~ 0
Audio debug
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 60218836
P 2350 6250
F 0 "J3" H 2322 6182 50  0000 R CNN
F 1 "Conn_01x03_Male" H 2322 6273 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 2350 6250 50  0001 C CNN
F 3 "~" H 2350 6250 50  0001 C CNN
	1    2350 6250
	1    0    0    1   
$EndComp
Wire Wire Line
	2550 6150 2850 6150
Wire Wire Line
	2550 6250 2750 6250
Wire Wire Line
	2750 6250 2750 6000
Wire Wire Line
	2550 6350 2750 6350
Wire Wire Line
	2750 6350 2750 6450
$Comp
L power:GND #PWR0122
U 1 1 6023B5EC
P 2750 6450
F 0 "#PWR0122" H 2750 6200 50  0001 C CNN
F 1 "GND" H 2755 6277 50  0000 C CNN
F 2 "" H 2750 6450 50  0001 C CNN
F 3 "" H 2750 6450 50  0001 C CNN
	1    2750 6450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0124
U 1 1 6023BC6A
P 2750 6000
F 0 "#PWR0124" H 2750 5850 50  0001 C CNN
F 1 "+5V" H 2765 6173 50  0000 C CNN
F 2 "" H 2750 6000 50  0001 C CNN
F 3 "" H 2750 6000 50  0001 C CNN
	1    2750 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1800 4200 1800
Wire Wire Line
	4200 1800 4200 1850
$Comp
L power:GND #PWR0112
U 1 1 60269FE0
P 4200 1850
F 0 "#PWR0112" H 4200 1600 50  0001 C CNN
F 1 "GND" H 4205 1677 50  0000 C CNN
F 2 "" H 4200 1850 50  0001 C CNN
F 3 "" H 4200 1850 50  0001 C CNN
	1    4200 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3950 1900 3950
Wire Wire Line
	1900 3950 1900 3900
Connection ~ 2100 3950
$Comp
L power:+5V #PWR0119
U 1 1 60274B2D
P 1900 3900
F 0 "#PWR0119" H 1900 3750 50  0001 C CNN
F 1 "+5V" H 1915 4073 50  0000 C CNN
F 2 "" H 1900 3900 50  0001 C CNN
F 3 "" H 1900 3900 50  0001 C CNN
	1    1900 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM_US RV1
U 1 1 60073645
P 1350 3250
F 0 "RV1" H 1282 3296 50  0000 R CNN
F 1 "R_POT_TRIM_US" H 1282 3205 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3314J_Vertical" H 1350 3250 50  0001 C CNN
F 3 "~" H 1350 3250 50  0001 C CNN
F 4 "C36376" H 1350 3250 50  0001 C CNN "LCSC"
	1    1350 3250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
